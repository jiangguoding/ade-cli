.. _install:

Installation
------------

Requirements
^^^^^^^^^^^^

* Docker 19.03 or newer, follow the `instructions for your OS`_
* If the host machine has an NVIDIA graphics card, install ``nvidia-container-toolkit``
    * See the `NVIDIA Docker's Repository`_ for more details


Installation
^^^^^^^^^^^^

Linux x86_64 and aarch64
========================

#. Verify that the Requirements above are fulfilled
#. To make ``ade`` available globally, install it somewhere in your ``PATH``:
   * ``echo $PATH`` will print a list of possible paths
   * Commonly used paths are ``~/.local/bin`` and ``/usr/local/bin``
#. Download the statically-linked binary from the `Releases`_ page of the ``ade-cli`` project: e.g. for ``x86_64``

.. code:: bash

   $ cd /path/from/step/above
   $ wget https://gitlab.com/ApexAI/ade-cli/-/jobs/1341322851/artifacts/raw/dist/ade+x86_64
   $ mv ade+x86_64 ade
   $ chmod +x ade
   $ ./ade --version
   4.3.0
   $ ./ade update-cli
   $ ./ade --version
   <latest-version>


For information on how to install ADE (``ade-cli``, ADE base image, and ADE volumes)
on an offline machine, see :ref:`offline-install`.

.. toctree::
   :maxdepth: 4
   :hidden:

   offline-install

Update
~~~~~~

To update ``ade-cli``, run ``ade update-cli``. If a newer version is available,
``ade`` will prompt for confirmation, download the new version, and replace itself.


OSX (Experimental)
==================
#. Verify that the Requirements above are fulfilled
#. Clone the repository and run the ``osx-install`` script:

.. code:: bash

   $ git clone https://gitlab.com/ApexAI/ade-cli
   $ cd ade-cli
   $ ./osx-install
   $ which ade
   ~/.local/bin/ade
   $ ade --version
   <version>

Update
~~~~~~

To update on OSX, go to the ``git`` clone, and run:

.. code:: bash

   $ git fetch
   $ git checkout <desired version>


Running X11 Apps on OSX
~~~~~~~~~~~~~~~~~~~~~~~

.. note::
   Running GUI apps on OSX is an experimental feature and requires some additional
   programs.

#. Install `Homebrew`_
#. Use Homebrew to install ``socat``:

   .. code:: bash

      $ brew install socat

#. Install XQuartz v2.7.11:

   .. code:: bash

      $ brew cask install xquartz

#. Restart your computer
#. Open XQuartz -> Preferences -> Security and check 'Allow connections from network clients'

   .. warning::
      The security implication of allowing connections from network clients has not been
      fully analyzed. Enable at your own risk.

#. Test UI apps: e.g. in an Ubuntu-based ADE image:

   .. code:: bash

      $ sudo apt-get update && sudo apt-get install -y x11-apps
      $ xeyes


Autocompletion
^^^^^^^^^^^^^^

To enable autocompletion, add the following to your ``.zshrc`` or ``.bashrc``:

   .. literalinclude:: ../completion.sh
      :language: shell
      :lines: 3-


See :ref:`usage` for next steps.

.. _NVIDIA Docker's Repository: https://github.com/NVIDIA/nvidia-docker
.. _Releases: https://gitlab.com/ApexAI/ade-cli/-/releases
.. _instructions for your OS: https://docs.docker.com/install/#server
.. _Homebrew: https://brew.sh/
